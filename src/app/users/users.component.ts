import { Component, OnInit } 	from '@angular/core';
import { ROUTER_DIRECTIVES }  from '@angular/router';
import { User }               from './user.model';
import { UserService }      	from './user.service';
import { HeaderComponent }    from "../shared/header.component";
import { SideMenuComponent }  from "../shared/sidemenu.component";

@Component({
  selector: 'users',
  templateUrl: '/app/users/users.component.html',
  providers: [ UserService ],
  directives: [ ROUTER_DIRECTIVES, HeaderComponent, SideMenuComponent ],
  styleUrls: ['/app/users/users.component.css']
})
export class UsersComponent implements OnInit {
  title    = 'Users';
  users = [];
  msgError: string;
  msgSuccess: string;

  constructor(private service: UserService) {}

  ngOnInit() {
    this.getUsers();
  }

  statusImage(user: User) {
    if(user.status == 'active') {
      return "/assets/images/ic_active.png";
    }
    return "/assets/images/ic_inactive.png";
  }

  getUsers() {
    this.service.getUsers()
                .subscribe(
                  users => this.users = users,
                  error => this.msgError = error);
  }

  state(user: User) {
    user.status = (user.status == 'active') ? 'inactive' : 'active';
    this.service.save(user)
                .subscribe(
                  user => this.msgSuccess = "Success",
                  error => this.msgError = error);
  }

  destroy(user: User) {
    if(confirm("Are you sure?")) {
      this.service.destroy(user)
                  .subscribe(
                    user => this.getUsers(),
                    error  => console.log(error));
    }
  }



}
