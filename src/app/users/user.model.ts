export class User {
  constructor(
    public id?: number,
    public name?: string,
    public role?: string,
    public user?: string,
    public status?: string,
    public createdAt?: string,
    public updatedAt?: string,
    public modules?: any
  ) { }
}