
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup,
         FormControl,
         REACTIVE_FORM_DIRECTIVES,
         Validators,
         FormBuilder,
         FormArray }                    from "@angular/forms";
import { Observable }                   from "rxjs/Rx";

import { Router, ActivatedRoute }       from '@angular/router';
import { User }                         from '../user.model';
import { UserService }                  from '../user.service';


declare var $:any;

@Component({
  moduleId: module.id,
  selector: 'user-form',
  templateUrl: '/app/users/user-form/user-form.component.html',
  directives: [REACTIVE_FORM_DIRECTIVES],
  providers: [UserService]
})
export class UserFormComponent implements OnInit, OnDestroy {
  title     = 'Users';
  roles     = ['admin', 'employee'];
  editable  = true;
  draggable = true;
  user: User;
  msgError: string;
  myForm: FormGroup;
  modules_labels = [
    'Branches',
    'Messages',
    'Users',
    'Reports'
  ];

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private service: UserService) {
    this.myForm = formBuilder.group({});
  }

  ngOnInit() {
     this.route.params.subscribe(params => {
       if (params['id'] !== undefined) {
         let id = +params['id'];
         this.service.getUser(id)
           .subscribe(
             user => this.user = user,
             error => console.log('Error: ' + error),
             () => this.recuperaModulos()
           );

         if(params['action'] === undefined) {
           this.editable = false;
         }

       } else {
         this.newUser();
       }
     });
     this.sanitize();
  }

  recuperaModulos() {
    let modules: Array<any> = [];
    let i: number = 0;
    let modules_bool: Array<any> = [[false],[false],[false],[false]];
    
    modules = this.user.modules;
    modules.forEach(element => {
      let pos:number = modules[i].id - 1;
      modules_bool[pos] = [true];
      i++;
    });
    this.myForm = this.formBuilder.group({
      'user': ['', [Validators.required]],
      'name': ['', [Validators.required]],
      'role': ['', [Validators.required]],
      'modules': this.formBuilder.array (
        modules_bool
      )
    });
  }

  ngOnDestroy() {
  }

  newUser() {
    this.user = new User();
    this.myForm = this.formBuilder.group({
      'user': ['', [Validators.required]],
      'name': ['', [Validators.required]],
      'role': ['', [Validators.required]],
      'modules': this.formBuilder.array (
        [[false],[false],[false],[false]]
      )
    });
  }

  onSubmit() {
    $("#loader").show();
    //let form = this.myForm.value;
    let form_str = JSON.stringify(this.myForm.value, null, 2);
    let form = JSON.parse(form_str);
    let modules: Array<number> = [];
    let i: number = 1;
    let id:number = 0;

    form.modules.forEach(element => {
      console.log(element);
      if(element) modules.push(i);
      i++;
    });
    form.modules = modules.join();

    if(this.user.id){
      id = this.user.id;
    }

    this.user = form;
    this.user.id = id;
    this.service.save(this.user)
                .subscribe(
                  user  => this.onClose(),
                  error => this.msgError = error);

  }

  onClose() {
    $("#myModal").modal('hide');
    $("#loader").hide();
    this.router.navigate(['/users']);
  }

  sanitize() {
    $("#myModal").modal('show');
    if(!this.editable) {
      $('button[type=submit]').hide();
    }
  }

}
