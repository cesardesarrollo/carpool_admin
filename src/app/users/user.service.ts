import { Injectable }     from "@angular/core"
import { Http, Response } from '@angular/http';
import { contentHeaders } from "../shared/headers";
import { Observable }     from 'rxjs/Rx';
import { User }           from './user.model';
import { Module }         from './module.model';
import { environment }    from '../../app/';
import { AuthHttp, 
         AuthConfig, 
         AUTH_PROVIDERS, JwtHelper } from 'angular2-jwt';

@Injectable()
export class UserService {
  base = environment.base_url_api + '/users';


  constructor(private http: Http, private authHttp: AuthHttp) { }

  getUsers(): Observable<User[]> {
    return this.authHttp.get(this.base, { headers: contentHeaders })
                    .map(res => res.json().users )
                    .catch(this.handleError);
  }

  getUser(id: number | string) {
    let url = `${this.base}/${id}`;
    return this.authHttp.get(url, { headers: contentHeaders }).map(res => res.json().user);
  }

  destroy(user: User) {
    let url = `${this.base}/${user.id}`;
    return this.authHttp
      .delete(url, { headers: contentHeaders })
      .map(res => res.json())
      .catch(this.handleError);
  }

  save(user: User) {
    if (user.id) {
      return this.put(user);
    }
    return this.post(user);
  }

  private post(user: User) {
    return this.authHttp.post(this.base, JSON.stringify(user), { headers: contentHeaders })
      .map(res => res.json())
      .catch(this.handleError);
  }

  private put(user: User) {
    let url = `${this.base}/${user.id}`;
    return this.authHttp.put(url, JSON.stringify(user), { headers: contentHeaders })
      .map(res => res.json())
      .catch(this.handleError);
  }

  private handleError (error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
