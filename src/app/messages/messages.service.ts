import { Injectable }     from "@angular/core"
import { Http, Response } from '@angular/http';
import { contentHeaders } from "../shared/headers";
import { Observable }     from 'rxjs/Rx';
import { Message }        from './message.model';
import { environment }    from '../../app/';
import { AuthHttp, 
         AuthConfig, 
         AUTH_PROVIDERS } from 'angular2-jwt';

@Injectable()
export class MessageService {
  base = environment.base_url_api + '/messages';

  constructor(private http: Http, private authHttp: AuthHttp) { }

  getMessages(): Observable<Message[]> {
    return this.authHttp.get(this.base, { headers: contentHeaders })
                    .map(res => res.json())
                    .catch(this.handleError);
  }

  getMessage(id: number | string) {
    let url = `${this.base}/${id}`;
    return this.authHttp.get(url, { headers: contentHeaders }).map(res => res.json());
  }

  destroy(message: Message) {
    let url = `${this.base}/${message.id}`;
    return this.authHttp
      .delete(url, { headers: contentHeaders })
      .toPromise()
      .catch(this.handleError);
  }

  save(message: Message): Observable<Message> {
    if (message.id) {
      return this.put(message);
    }
    return this.post(message);
  }

  private post(message: Message): Observable<Message> {
    return this.authHttp.post(this.base, JSON.stringify(message), { headers: contentHeaders })
      .map(res => res.json())
      .catch(this.handleError);
  }

  private put(message: Message): Observable<Message> {
    let url = `${this.base}/${message.id}`;
    return this.authHttp.put(url, JSON.stringify(message), { headers: contentHeaders })
      .map(res => res.json())
      .catch(this.handleError);
  }

  private handleError (error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
