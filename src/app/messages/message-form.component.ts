import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm }            from '@angular/forms';
import { Message }           from './message.model';
import { MessageService }    from './messages.service';
import { Router,
         ActivatedRoute,
         ROUTER_DIRECTIVES }  from '@angular/router';
declare var $:any;

@Component({
  selector: 'message-form',
  templateUrl: '/app/messages/message-form.component.html',
  directives: [ROUTER_DIRECTIVES],
  providers: [MessageService]
})

export class MessageFormComponent implements OnInit {
  title    = 'Message';
  name     = 'Title';
  editable = true;
  states   = ['active', 'inactive']
  roles    = ['driver', 'passenger']
  message: Message;
  msgError: string;

  constructor(
     private route: ActivatedRoute,
     private router: Router,
     private service: MessageService) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params['id'] !== undefined) {
        let id = +params['id'];
        this.service.getMessage(id)
          .subscribe(
            message => this.message = message,
            error => console.log('Error: ' + error)
          );

        if(params['action'] === undefined) {
          this.editable = false;
        }
      } else {
        this.message = new Message();
      }
    });
    this.show();
  }

  sanitize() {
    if(!this.editable) {
      $('button[type=submit]').hide();
    }
  }

  onSubmit() {
    $("#loader").show();
    this.service.save(this.message)
                .subscribe(
                  message => this.onClose(),
                  error => this.msgError = error);
  }

  show() {
    this.sanitize();
    $("#myModal").modal('show');
  }

  onClose() {
    $("#myModal").modal('hide');
    $("#loader").hide();
    this.router.navigate(['/messages']);
  }
}
