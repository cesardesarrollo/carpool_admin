export class Message {
  constructor(
    public id?: number,
    public name?: string,
    public language?: string,
    public description?: string,
    public role?: string,
    public status?: string,
    public createdAt?: string,
    public updatedAt?: string
  ) { }
}
