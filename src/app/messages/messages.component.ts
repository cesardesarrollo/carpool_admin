import { Component, OnInit } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { Message }           from './message.model';
import { MessageService }    from './messages.service';
import { HeaderComponent }    from "../shared/header.component";
import { SideMenuComponent }  from "../shared/sidemenu.component";

@Component({
  selector: 'messages',
  templateUrl: '/app/messages/messages.component.html',
  directives: [ ROUTER_DIRECTIVES, HeaderComponent, SideMenuComponent ],
  providers: [ MessageService ]
})

export class MessagesComponent implements OnInit {
  title       = 'MESSAGES';
  add         = 'ADD MESSAGE';
  name        = 'Title';
  role        = 'Role';
  language    = 'Language';
  description = 'Description';
  status      = 'Status';
  options     = 'Options';
  messages    = [];
  msgError: string;
  msgSuccess: string;

  constructor(
    private service: MessageService) {
  }

  ngOnInit() {
    this.getMessages();
  }

  statusImage(message: Message) {
    if(message.status == 'active') {
      return "/assets/images/ic_active.png";
    }
    return "/assets/images/ic_inactive.png";
  }

  getMessages() {
    this.service.getMessages()
                .subscribe(
                  messages => this.messages = messages,
                  error => this.msgError = error);
  }

  state(message: Message) {
    message.status = (message.status == 'active') ? 'inactive' : 'active';
    this.service.save(message)
                .subscribe(
                  message => this.msgSuccess = "Success",
                  error => this.msgError = error);
  }

  destroy(message: Message) {
    if(confirm("Are you sure?")) {
      this.service.destroy(message);
      this.getMessages();
    }
  }
}
