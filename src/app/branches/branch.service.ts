import { Injectable }     from "@angular/core"
import { Http, Response } from '@angular/http';
import { contentHeaders } from "../shared/headers";
import { Observable }     from 'rxjs/Rx';
import { Branch }         from './branch.model';
import { environment }    from '../../app/';
import { AuthHttp, 
         AuthConfig, 
         AUTH_PROVIDERS } from 'angular2-jwt';

@Injectable()
export class BranchService {
  base = environment.base_url_api + '/branches';

  constructor(private http: Http, private authHttp: AuthHttp) { }

  getBranches(): Observable<Branch[]> {
    return this.authHttp.get(this.base, { headers: contentHeaders })
                    .map(res => res.json().branches)
                    .catch(this.handleError);
  }

  getBranch(id: number | string) {
    let url = `${this.base}/${id}`;
    return this.authHttp.get(url, { headers: contentHeaders }).map(res => res.json());
  }

  destroy(branch: Branch) {
    let url = `${this.base}/${branch.id}`;
    return this.authHttp
      .delete(url, { headers: contentHeaders })
      .map(res => res.json())
      .catch(this.handleError);
  }

  save(branch: Branch) {
    if (branch.id) {
      return this.put(branch);
    }
    return this.post(branch);
  }

  private post(branch: Branch) {
    return this.authHttp.post(this.base, JSON.stringify(branch), { headers: contentHeaders })
      .map(res => res.json())
      .catch(this.handleError);
  }

  private put(branch: Branch) {
    let url = `${this.base}/${branch.id}`;
    return this.authHttp.put(url, JSON.stringify(branch), { headers: contentHeaders })
      .map(res => res.json())
      .catch(this.handleError);
  }

  private handleError (error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
