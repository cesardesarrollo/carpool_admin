/* tslint:disable:no-unused-variable */

import {
  beforeEach, beforeEachProviders,
  describe, xdescribe,
  expect, it, xit,
  async, inject
} from '@angular/core/testing';
import { BranchService } from './branch.service';

describe('Branch Service', () => {
  beforeEachProviders(() => [BranchService]);

  it('should ...',
      inject([BranchService], (service: BranchService) => {
    expect(service).toBeTruthy();
  }));
});
