import { Component, OnInit, OnDestroy }  from '@angular/core';
import { NgForm }                  from '@angular/forms';
import { Router, ActivatedRoute }  from '@angular/router';
import { Branch }                  from '../branch.model';
import { BranchService }           from '../branch.service';
import {
  MapsAPILoader,
  NoOpMapsAPILoader,
  MouseEvent,
  GOOGLE_MAPS_PROVIDERS,
  GOOGLE_MAPS_DIRECTIVES
} from 'angular2-google-maps/core';
declare var $:any;

export interface Marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}

@Component({
  selector: 'branch-form',
  templateUrl: '/app/branches/branch-form/branch-form.component.html',
  providers: [BranchService, GOOGLE_MAPS_PROVIDERS],
  directives: [GOOGLE_MAPS_DIRECTIVES]
})

export class BranchFormComponent implements OnInit, OnDestroy  {
  title     = 'Branches';
  states    = ['active', 'inactive']
  editable  = true;
  draggable = true;
  branch: Branch;
  lat: number  = 20.7422717;
  lng: number  = -103.4492999;
  zoom: number = 10;
  branchMarker: Marker;
  msgError: string;

  constructor(
   private route: ActivatedRoute,
   private router: Router,
   private service: BranchService) {}

  ngOnInit() {
     this.route.params.subscribe(params => {
       if (params['id'] !== undefined) {
         let id = +params['id'];
         this.service.getBranch(id)
           .subscribe(
             branch => this.branch = branch,
             error => console.log('Error: ' + error),
             () => this.addMarker()
           );

         if(params['action'] === undefined) {
           this.editable = false;
         }
       } else {
         this.newBranch();
       }
     });
     this.sanitize();
  }

  ngOnDestroy() {

  }

  newBranch() {
    this.branch = new Branch();
    this.branch.latitude  = this.lat;
    this.branch.longitude = this.lng;
    this.addMarker()
  }

  addMarker() {
    if(this.branch.latitude == 0)
      this.branch.latitude = this.lat;

    if(this.branch.longitude == 0)
     this.branch.longitude = this.lng;

    this.branchMarker = {
      lat: this.branch.latitude,
      lng: this.branch.longitude,
      label: 'F',
      draggable: true
     }
  }

  private onSubmit() {
    $("#loader").show();
    this.service.save(this.branch)
                .subscribe(
                  branch => this.onClose(),
                  error  => this.msgError = error);
  }

  private onClose() {
    $("#myModal").modal('hide');
    $("#loader").hide();
    this.router.navigate(['/branches']);
  }

  private sanitize() {
    $("#myModal").modal('show');
    if(!this.editable) {
      $('button[type=submit]').hide();
    }
  }

  private markerDragEnd($event: MouseEvent) {
    this.branch.latitude = $event.coords.lat;
    this.branch.longitude = $event.coords.lng;
  }
}
