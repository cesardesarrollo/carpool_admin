export class Branch {
  constructor(
    public id?: number,
    public name?: string,
    public address?: string,
    public latitude?: number,
    public longitude?: number,
    public status?: string,
    public createdAt?: string,
    public updatedAt?: string
  ) { }
}
