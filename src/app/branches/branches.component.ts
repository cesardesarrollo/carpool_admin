import { Component, OnInit }  from '@angular/core';
import { ROUTER_DIRECTIVES }  from '@angular/router';
import { Branch }             from './branch.model';
import { BranchService }      from './branch.service';

import { HeaderComponent }    from "../shared/header.component";
import { SideMenuComponent }  from "../shared/sidemenu.component";

@Component({
  selector: 'branches',
  templateUrl: '/app/branches/branches.component.html',
  providers: [ BranchService ],
  directives: [ ROUTER_DIRECTIVES, HeaderComponent, SideMenuComponent ]
})

export class BranchesComponent implements OnInit {
  title    = 'Branches';
  branches = [];
  msgError: string;
  msgSuccess: string;

  constructor(
    private service: BranchService) {
  }

  ngOnInit() {
    this.getBranches();
  }

  statusImage(branch: Branch) {
    if(branch.status == 'active') {
      return "/assets/images/ic_active.png";
    }
    return "/assets/images/ic_inactive.png";
  }

  getBranches() {
    this.service.getBranches()
                .subscribe(
                  branches => this.branches = branches,
                  error => this.msgError = error);
  }

  state(branch: Branch) {
    branch.status = (branch.status == 'active') ? 'inactive' : 'active';
    this.service.save(branch)
                .subscribe(
                  branch => this.msgSuccess = "Success",
                  error => this.msgError = error);
  }

  destroy(branch: Branch) {
    if(confirm("Are you sure?")) {
      this.service.destroy(branch)
                  .subscribe(
                    branch => this.getBranches(),
                    error  => console.log(error));
    }
  }
}
