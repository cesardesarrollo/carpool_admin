// The file for the current environment will overwrite this one during build
// Different environments can be found in config/environment.{dev|prod}.ts
// The build system defaults to the dev environment

export const environment = {
  production: false,
  base_url_api: 'https://flex-carpool.herokuapp.com/api',
  google_maps_key: 'AIzaSyCwI-lTPo9wp_rHSUf9EfS3iQwqvGd9MVY'
};
