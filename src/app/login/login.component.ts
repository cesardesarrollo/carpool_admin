import { Component, OnInit }  from "@angular/core";
import { HeaderComponent }    from "../shared/header.component";
import { SideMenuComponent }  from "../shared/sidemenu.component";
import { AuthService }        from "../shared/auth.service";
import { FormBuilder, FormGroup, Validators, REACTIVE_FORM_DIRECTIVES } from "@angular/forms";

@Component({
  selector: 'login',
  template: `
    <div class="outlet_body">
      <form id="login" class="form-signin text-center" [formGroup]="myForm" (ngSubmit)="onSignin()">
        <img class="logo" src="assets/images/logo2.png" alt="Carpool">
        <label for="user" class="sr-only">User</label>
        <input formControlName="user" type="text" id="user" class="form-control" placeholder="User" required autofocus>
        <label for="password" class="sr-only">Password</label>
        <input formControlName="password" type="password" id="password" class="form-control" placeholder="Password" required>
          <label>
            <a href="#">Forgot your password?</a>
          </label>
        <button type="submit" [disabled]="!myForm.valid" class="btn btn-lg btn-block btn-login">LOG IN</button>
      </form>
    </div>
  `
  ,
  styleUrls: [ './app/login/login.component.css' ],
  directives: [ REACTIVE_FORM_DIRECTIVES ]
})
export class Login implements OnInit {
  constructor(private fb: FormBuilder, private authService: AuthService) {}

  myForm: FormGroup;

  onSignin() {
    this.authService.signinUser(this.myForm.value);
  }
  
  ngOnInit():any {
      this.myForm = this.fb.group({
          user: ['', Validators.required],
          password: ['', Validators.required],
      });
  }

}
