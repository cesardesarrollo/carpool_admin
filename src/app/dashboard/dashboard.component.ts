import { Component, OnInit } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { HeaderComponent }    from "../shared/header.component";
import { SideMenuComponent }  from "../shared/sidemenu.component";

@Component({
  selector: 'dashboard',
  templateUrl: '/app/dashboard/dashboard.component.html',
  styleUrls: [ '/app/dashboard/dashboard.component.css' ],
  directives: [ ROUTER_DIRECTIVES, HeaderComponent, SideMenuComponent ]
})

export class DashboardComponent implements OnInit {

  constructor() {}

  ngOnInit() {
  }
}
