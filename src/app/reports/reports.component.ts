import { Component, OnInit } from '@angular/core';
import { HeaderComponent }    from "../shared/header.component";
import { SideMenuComponent }  from "../shared/sidemenu.component";

@Component({
  moduleId: module.id,
  selector: 'app-reports',
  templateUrl: '/app/reports/reports.component.html',
  directives: [ HeaderComponent, SideMenuComponent ],
  styleUrls: ['/app/reports/reports.component.css']
})
export class ReportsComponent implements OnInit {

  constructor() {}

  ngOnInit() {
  }

}
