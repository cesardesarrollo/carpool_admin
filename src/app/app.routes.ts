import { provideRouter, RouterConfig }  from '@angular/router';
import { DashboardComponent }           from './dashboard/dashboard.component';
import { ReportsComponent }             from './reports/reports.component';
import { Login }                        from './login/';
import { AuthGuard }                    from './shared/auth.guard';

import { UsersComponent, UserFormComponent }       from './users/';
import { MessagesComponent, MessageFormComponent } from './messages/';
import { BranchesComponent, BranchFormComponent }  from './branches/';
export const routes: RouterConfig = [

  { path: '', component:  Login },
  { path: 'login', component: Login }, 
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'users', component: UsersComponent, canActivate: [AuthGuard] },
  { path: 'users/new', component: UserFormComponent, canActivate: [AuthGuard] },
  { path: 'users/:id', component: UserFormComponent, canActivate: [AuthGuard] },
  { path: 'users/:id/:action', component: UserFormComponent, canActivate: [AuthGuard] },
  { path: 'reports', component: ReportsComponent, canActivate: [AuthGuard]},
  { path: 'messages', component: MessagesComponent, canActivate: [AuthGuard] },
  { path: 'messages/new', component: MessageFormComponent, canActivate: [AuthGuard] },
  { path: 'messages/:id', component: MessageFormComponent, canActivate: [AuthGuard] },
  { path: 'messages/:id/:action', component: MessageFormComponent, canActivate: [AuthGuard] },
  { path: 'branches', component: BranchesComponent, canActivate: [AuthGuard] },
  { path: 'branches/new', component: BranchFormComponent, canActivate: [AuthGuard] },
  { path: 'branches/:id', component: BranchFormComponent, canActivate: [AuthGuard] },
  { path: 'branches/:id/:action', component: BranchFormComponent, canActivate: [AuthGuard]  },
  { path: '**', component: Login }

];

export const APP_ROUTER_PROVIDERS = [
  provideRouter(routes)
];
