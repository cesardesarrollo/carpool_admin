import { Component, OnInit } from "@angular/core";
import { ROUTER_DIRECTIVES } from "@angular/router";

import { User }              from '../users/user.model';
import { AuthService }       from "./auth.service";

@Component({
    selector: 'side-menu',
    template: `
      <div class="col-sm-3 col-md-2 sidebar">
        <ul class="nav nav-sidebar">
          <li *ngFor="let module of user_modules" >
            <a [routerLinkActive]="['active']" [routerLink]=" [ module.path ] ">
              <img src="assets/images/icons/{{module.name}}.png"/>
              {{module.name}}
            </a>
          </li> 
        </ul>
      </div>
    `,
  directives: [ROUTER_DIRECTIVES]
})
export class SideMenuComponent implements OnInit {
  user_modules = [];

  constructor(private authService: AuthService) {}

  ngOnInit() {
    this.getUserModules();
  }

  getUserModules() {
    this.user_modules = JSON.parse(this.authService.getUserModules());
  }

}





