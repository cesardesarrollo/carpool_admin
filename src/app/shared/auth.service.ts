import { Injectable }     from "@angular/core";
import { User }           from "./user.interface";
import { contentHeaders } from "./headers";
import { Http, Headers }  from "@angular/http";
import { Router }         from "@angular/router";
import { environment }    from "../../app/";

import { AuthHttp, 
         AuthConfig, 
         AUTH_PROVIDERS, 
         JwtHelper }      from 'angular2-jwt';

@Injectable()
export class AuthService {
  base = environment.base_url_api + '/auth/sign_in';
  jwtHelper: JwtHelper = new JwtHelper();

  constructor(private router: Router, public http: Http ) {}

  signinUser(user: User) {
    let body = JSON.stringify({ user: user.user, password: user.password });
    this.http.post(this.base, body, { headers: contentHeaders })
      .subscribe(
        response => {

          //Módulos permitidos a usuario
          let modules = [];
          response.json().modules.forEach(element => {
            let path = '/' + element.name.toLowerCase();
            modules.push({"name": element.name, "id": element.id, "path": path});
          });

          this.deleteLocalStorage;

          localStorage.setItem('id_token', response.json().user.token);
          localStorage.setItem('user_id', response.json().user.id);
          localStorage.setItem('user_name', response.json().user.name);
          localStorage.setItem('user_modules', JSON.stringify(modules) );
          this.router.navigate(['/dashboard']);
        },
        error => {
          alert(error.text());
          console.log(error.text());
        }
      );
  }

  deleteLocalStorage() {
    localStorage.removeItem('id_token');
    localStorage.removeItem('user_id');
    localStorage.removeItem('user_name');
    localStorage.removeItem('user_role');
    localStorage.removeItem('user_modules');
  }

  logout() {
    this.deleteLocalStorage;
    this.router.navigate(['/login']);
  }
  
  getUserName() {
    let user_name = localStorage.getItem('user_name');
    if (user_name) {
      return user_name;
    } else {
      return 'Guest';
    }
  }

  getUserId() {
    let user_id = localStorage.getItem('user_id');
    if (user_id) {
      return user_id;
    } else {
      return 0;
    }
  }

  getUserModules() {
    let user_modules = localStorage.getItem('user_modules');
    if (user_modules) {
      return user_modules;
    } else {
      return 0;
    }
  }

  getUserModulesPaths() {
    let user_modules = JSON.parse(localStorage.getItem('user_modules'));
    let modules: Array<string> = [];
    user_modules.forEach(element => {
      modules.push(element.path);
    });
    return modules;
  }

  useJwtHelper() {
    var token = localStorage.getItem('id_token');
    return this.jwtHelper.decodeToken(token);
  }

  isAuthenticated() {
    var id_token  = localStorage.getItem('id_token');
    if (id_token) {
      return true;
    } else {
      return false;
    }
  }

  modulePermission(module_path: string) {
    if(module_path !== '/dashboard') {
      let modules = this.getUserModulesPaths();
      let path: any = module_path.split('/');
      if (modules.indexOf('/' + path[1]) >= 0){
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

}
