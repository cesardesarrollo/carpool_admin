import { Component } from "@angular/core";
import { ROUTER_DIRECTIVES } from "@angular/router";

import { AuthService } from "./auth.service";

@Component({
    selector: 'app-header',
    template: `
        <header>
          <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
              <div class="row">
                <div class="col-sm-3 col-md-2 logo">
                  <img id="logoImage" src="assets/images/logo.png" class="img-responsive center-block"/>
                </div>
                <div class="col-sm-9 col-md-10">
                  <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </button>
                  </div>
                  <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                      <li>
                        <span class="current-user">
                          Welcome <b>{{username}}</b>
                        </span>
                      </li>
                      <li>
                        <a href="#">
                          <img src="assets/images/default-avatar.png" class="avatar"/>
                        </a>
                      </li>
                      <li *ngIf="!isAuth()" class="btn-round"> <a [routerLink]="['login']">LOGIN</a> </li>
                      <li *ngIf="isAuth()">
                         <a (click)="onLogout()" style="cursor: pointer" class="btn-round logout">LOGOUT</a> 
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </nav>
        </header>
    `,
  directives: [ROUTER_DIRECTIVES]
})
export class HeaderComponent {
  constructor(private authService: AuthService) {}

  isAuth() {
    return this.authService.isAuthenticated();
  }

  onLogout() {
    this.authService.logout();
  }

  username =  this.authService.getUserName();

}
