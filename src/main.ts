import { bootstrap }                  from '@angular/platform-browser-dynamic';
import { LocationStrategy,
         HashLocationStrategy,
         APP_BASE_HREF }              from '@angular/common';
import { GOOGLE_MAPS_PROVIDERS,
        LazyMapsAPILoaderConfig }     from 'angular2-google-maps/core';
import { provide, enableProdMode }    from '@angular/core';
import { AppComponent, environment }  from './app/';
import { APP_ROUTER_PROVIDERS }       from './app/app.routes';
import { HTTP_PROVIDERS, RequestOptions, Http }       
                                      from '@angular/http';
import { provideForms, disableDeprecatedForms, ReactiveFormsModule } 
                                      from "@angular/forms";
import { AUTH_PROVIDERS, AuthHttp, AuthConfig }             
                                      from 'angular2-jwt';
import { AuthService }                from "./app/shared/auth.service";
import { AuthGuard }                  from './app/shared/auth.guard';

if (environment.production) {
  enableProdMode();
}

bootstrap(AppComponent, [
  APP_ROUTER_PROVIDERS,
  HTTP_PROVIDERS,
  AUTH_PROVIDERS,
  provide(AuthHttp, {
    useFactory: (http) => {
      return new AuthHttp(new AuthConfig({
        headerPrefix: '',
        noTokenScheme: true
      }), http);
    },
    deps: [Http]
  }),
  AuthGuard,
  AuthService,
  disableDeprecatedForms(),
  provideForms(),

  ReactiveFormsModule,
  
  GOOGLE_MAPS_PROVIDERS,
  provide(LazyMapsAPILoaderConfig, {useFactory: () => {
    let config = new LazyMapsAPILoaderConfig();
    config.apiKey = environment.google_maps_key;
    return config;
  }})
]);
