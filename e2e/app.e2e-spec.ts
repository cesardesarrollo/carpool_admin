import { CarpoolPage } from './app.po';

describe('carpool App', function() {
  let page: CarpoolPage;

  beforeEach(() => {
    page = new CarpoolPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
